import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class Baidufenci {
    public static void main(String[] args) throws IOException {
        Boolean flag = false;
       String[] ciku = new String[110000];
        Scanner sc = new Scanner(System.in);
        BufferedReader br = new BufferedReader(new FileReader("src/百度分词词库.txt"));
        String read;
        for (int i = 0 ; (read = br.readLine()) != null ; i++)
        {
            ciku[i] = read;
        }
        while (true) {
            System.out.println("请输入你想查询的词组");
            System.out.println("如果想退出请输入 0 ");
            read = sc.nextLine();
            if (read.equals("0"))
                break;
            for (int i = 0; (ciku[i] != null); i++) {
                if(ciku[i].equals(read))
                {
                    flag = true;
                    System.out.println("存在该词组");
                    System.out.println("=============");
                    break;
                }
            }
            if(!flag)
            {
                System.out.println("sorry，没有你想的查询词组");
                System.out.println("=============");
            }
            flag = false;
        }
    }
}
