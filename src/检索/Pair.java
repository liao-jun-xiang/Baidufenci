package 检索;

public class Pair <T,F> {
    public T first;
    public F second;

    public Pair(T first, F second) {
        this.first = first;
        this.second = second;
    }

    public static <T,F> Pair make_Pair (T first, F second){
        return new Pair(first,second);
    }
}
