package 检索;

import java.io.*;
import java.util.Arrays;
import java.util.Scanner;

public class Demo {
    public static void main(String[] args) throws IOException {
        BufferedReader bfr = new BufferedReader(new FileReader("src\\百度分词词库.txt"));
        Pair<String,Integer>[] arr = new Pair[110000];
        String s;int index = 0;
        while ((s = bfr.readLine())!=null){
            arr[index++] = new Pair<>(s,s.hashCode());
        }
        //快速排序
        Arrays.sort(arr,((o1, o2) -> o1 == null || o2 == null ? 0 :(o1.value < o2.value ? -1:1) ));

        Scanner sc = new Scanner(System.in);
        while(!(s = sc.next()).equals("exit")){
            System.out.println(search_equals_hash(arr,binary_search(arr,0,index-1,s.hashCode()),index-1,s));
        }

    }
//比较hash值相同但字符不同的情况
    public static boolean search_equals_hash(Pair<String,Integer>[] arr,int start,int last,String find){
        if(start==-1)return false;
        int left = start,right = start;
        for(;left > 0 & arr[left].value == arr[start].value;--left);
        for(;right < last & arr[right].value == arr[start].value;++right);
        for(int i = left;i<=right;++i){
            if(arr[i].string.equals(find))return true;
        }
        return false;
    }
    //二分查找
    public static int binary_search(Pair<String,Integer>[] arr,int beg,int end,int hashcode){
        if (beg > end)return -1;
        int half = (beg+end)/2;
        if(arr[half].value == hashcode)return half;
        if(arr[half].value > hashcode)return binary_search(arr,beg,half-1,hashcode);
        return binary_search(arr,half+1,end,hashcode);
    }
}
